const ans = require('../limitFunctionCallCount.cjs');

function cb(...args){
        console.log(`I am printing this.`);
        console.log(args);
}
try {
    const res = ans(cb,5);
    for(let index = 0;index<5;index++){
        res();
    }
    res();
} catch (error) {
    console.log(error);
}

try {
    const res = ans();
} catch (error) {
    console.log(error);
}

try {
    const res = ans(cb,-1);
} catch (error) {
    console.log(error);
}
