const ans = function limitFunctionCallCount(cb,num){

    if (num <0 || typeof cb !== 'function' || typeof num !== 'number') {
        throw new Error('Wrong inputs has been passed to the function.');
      }

    let count = 0;
    function demo(...args){
            if(count < num){
                count++;
                return cb(...args);
            }
            else{
                return null;
            }
        }
    return demo;
}
module.exports = ans;