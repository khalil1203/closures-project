const ans = function counterFactory(){
    let data = 0;
    
    function increment(){
        return ++data;
    }

    function decrement(){
        return --data;
    }

    return {increment,decrement};
}

module.exports = ans;