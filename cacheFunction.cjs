const ans = function cacheFunction(cb){
    if (typeof cb !== 'function'){
        throw new Error("Please use function in the parameter!!!");
    }

    let cache = {};
    function remote(...args){
        const result = JSON.stringify(args);
        if(result in cache){
            console.log(cache);
            return cache[result];
        }
        else{
            const demo = cb(...args);
            cache[result] = demo;
            console.log(cache);
            return demo;
        }
    }
    return remote;

}

module.exports = ans;